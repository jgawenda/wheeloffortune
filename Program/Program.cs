﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WheelOfFortune;

namespace Program
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();
            game.Start();

            Console.Clear();
            Console.WriteLine("Thanks for playing");
            Console.Write("Copyright 2018 Jakub Gawenda");
            Console.ReadLine();
        }
    }
}
