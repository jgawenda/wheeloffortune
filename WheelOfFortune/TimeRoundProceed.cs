﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Timers;

namespace WheelOfFortune
{
    class TimeRoundProceed
    {
        private int _cashToWinHere;
        private string _prizeToWinHere;
        private MagicPhrase _magicPhrase;
        private List<PlayerAccount> _playerAccounts;
        private bool[] _usersPermitToAnswer;
        private Thread _thisThread;
        private Thread _workerThread;
        private RevealRandomLetters _workerObject;
        private System.Timers.Timer _timer;
        private bool _userHasTimeToSolve;

        private void PrepareUserPermits(int numberOfUsers)
        {
            _usersPermitToAnswer = new bool[numberOfUsers];

            for (int i=0; i<numberOfUsers; i++)
            {
                _usersPermitToAnswer[i] = true;
            }
        }

        public TimeRoundProceed(MagicPhrase magicPhrase, List<PlayerAccount> playerAccounts, int cashToWin, string prizeToWin)
        {
            _magicPhrase = magicPhrase;
            _playerAccounts = playerAccounts;
            _cashToWinHere = cashToWin;
            _prizeToWinHere = prizeToWin;
            _userHasTimeToSolve = true;

            PrepareUserPermits(_playerAccounts.Count);
        }

        public void AddThread(Thread thread)
        {
            _thisThread = thread;
        }

        public void Begin()
        {
            _workerObject = new RevealRandomLetters(_magicPhrase);
            _workerThread = new Thread(new ThreadStart(_workerObject.AutoRevealLetters));
            _workerThread.Start();

            _timer = new System.Timers.Timer();
            _timer.Elapsed += Timer_Elapsed;

            while (!_magicPhrase.IsPhraseSolved && AreThereAnyPermissionsYet())
            {
                if (Console.KeyAvailable)
                {
                    var key = Console.ReadKey();
                    if (key.Key == ConsoleKey.A && _usersPermitToAnswer[0] == true)
                    {
                        ProceedGuessing(_playerAccounts.ElementAt(0));
                    }
                    else if (key.Key == ConsoleKey.G && _usersPermitToAnswer[1] == true)
                    {
                        ProceedGuessing(_playerAccounts.ElementAt(1));
                    }
                    else if (key.Key == ConsoleKey.L && _usersPermitToAnswer[2] == true)
                    {
                        ProceedGuessing(_playerAccounts.ElementAt(2));
                    }
                }
            }

            if (!AreThereAnyPermissionsYet())
            {
                _magicPhrase.SolveThePhraseAutomatically();
                //Console.Clear();
                Console.WriteLine("There was no winner in this round...");
            }

            _workerThread.Join();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _userHasTimeToSolve = false;
            System.Timers.Timer timer = (System.Timers.Timer)sender;
            timer.Stop();
            
            Console.Beep();
            Console.WriteLine("\n\nTime's UP.");
        }

        private void ProceedGuessing(PlayerAccount player)
        {
            _userHasTimeToSolve = true;

            Console.Clear();
            Console.WriteLine("\n");
            _magicPhrase.WriteProgress();
            Console.WriteLine("\n\n");

            _workerObject.RequestPause();
            _timer.Interval = 15000;
            _timer.Start();

            Console.BackgroundColor = player.Color;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("Player {0} is solving the puzzle.", player.Name);
            Console.ResetColor();

            Console.WriteLine("You have 15 seconds.");
            Console.Write("Enter correct phrase: ");

            string userGuess = Console.ReadLine();

            if (_userHasTimeToSolve && _magicPhrase.SolveThePhrase(userGuess))
            {
                _timer.Stop();
                Console.Clear();
                Console.WriteLine("CONGRATULATIONS!\n\n");

                _magicPhrase.WriteProgress();

                Console.WriteLine("\n\n");

                RewardTheWinner(player);
            }
            else
            {
                _timer.Stop();
                _usersPermitToAnswer[_playerAccounts.IndexOf(player)] = false;

                if (_userHasTimeToSolve)
                    Console.WriteLine("Wrong phrase!");
            }

            Console.ReadLine();
            _workerObject.RequestResume();
        }

        private void RewardTheWinner(PlayerAccount player)
        {
            player.AddCashTemporarily(_cashToWinHere);

            if (_prizeToWinHere != "")
                player.TempPrizes.AddPrize(_prizeToWinHere);

            player.CopyTempPrizesToTotalAndDelete();
        }

        private bool AreThereAnyPermissionsYet()
        {
            foreach (bool value in _usersPermitToAnswer)
            {
                if (value == true)
                    return true;
            }

            return false;
        }
    }
}
