﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WheelOfFortune
{
    abstract class Round
    {
        protected List<PlayerAccount> _playerAccounts;
        protected MagicPhrase _magicPhrase;

        protected void RemoveAllTempPrizes()
        {
            foreach (PlayerAccount player in _playerAccounts)
            {
                player.TempPrizes.RemoveCash();
                player.TempPrizes.RemovePrizes();
            }
        }

        protected void DisplayPlayersInfo()
        {
            int cursorLeft = 0;
            int cursorTop = Console.CursorTop;

            foreach (PlayerAccount player in _playerAccounts)
            {
                player.DisplayAllInfo(cursorLeft, cursorTop);
                cursorLeft += 25;
            }
        }

        protected void DisplayPlayersTotals()
        {
            int cursorLeft = 0;
            int cursorTop = Console.CursorTop;

            foreach (PlayerAccount player in _playerAccounts)
            {
                player.DisplayTotals(cursorLeft, cursorTop);
                cursorLeft += 25;
            }
        }

        protected abstract void DisplayRoundName();
        
        public abstract void Start();
    }
}
