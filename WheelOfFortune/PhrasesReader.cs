﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace WheelOfFortune
{
    class PhrasesReader
    {
        private List<string[]> _listOfPhrasesToUse = new List<string[]>();

        public PhrasesReader(string filePath)
        {
            LoadArrayToList(filePath);
        }

        public string[] GetNextRandomPhrase()
        {
            if (_listOfPhrasesToUse.Count < 1)
                return new string[] { "", "" };

            Random random = new Random();

            int randomNumber = random.Next(0, _listOfPhrasesToUse.Count());

            string[] tempPhrase = _listOfPhrasesToUse.ElementAt(randomNumber);

            _listOfPhrasesToUse.RemoveAt(randomNumber);

            return tempPhrase;
        }

        private string[] LoadPhrasesFromFile(string filePath)
        {
            try
            {
                string[] phrasesFromFile = File.ReadAllLines(filePath, Encoding.GetEncoding("Windows-1250"));

                Console.WriteLine("File opened succesfully");

                return phrasesFromFile;
            }
            catch (Exception ex)
            {
                Console.Clear();
                Console.WriteLine("Error: {0}", ex.Message);
                Console.WriteLine("Press enter to try again");
                return new string[] { ";" };
            }
        }

        private void LoadArrayToList(string filePath)
        {
            string[] phrasesArray = LoadPhrasesFromFile(filePath);

            string[] tempArray;

            foreach (string phrase in phrasesArray)
            {
                tempArray = phrase.Split(';');
                _listOfPhrasesToUse.Add(new string[] { tempArray[0], tempArray[1] });
            }
        }
        
    }
}
