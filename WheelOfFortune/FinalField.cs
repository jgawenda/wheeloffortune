﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WheelOfFortune
{
    class FinalField
    {
        public int Value { get; private set; }
        public string Prize { get; private set; }

        public FinalField()
        {
            Value = 0;
            Prize = "";
        }

        public FinalField(int value) : this ()
        {
            Value = value;
        }

        public FinalField(string prize) : this ()
        {
            Prize = prize;
        }

        public void Reveal()
        {
            if (Value > 0)
                Console.WriteLine("${0}!", Value);
            else
                Console.WriteLine("{0}!", Prize);
        }
    }
}
