﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WheelOfFortune
{
    class PlayerAccount
    {
        public string Name { get; private set; }
        public ConsoleColor Color { get; private set; }
        public int TotalCash { get; private set; }
        public List<string> TotalPrizes { get; private set; }

        public TemporaryPrizes TempPrizes { get; private set; }

        public int TotalBankruptcies { get; private set; }

        public PlayerAccount(string name, ConsoleColor color)
        {
            Name = name;
            Color = color;
            TotalCash = 0;
            TempPrizes = new TemporaryPrizes();
            TotalPrizes = new List<string>();
            TotalBankruptcies = 0;
        }

        public void AddCashTemporarily(int amount)
        {
            TempPrizes.AddCash(amount);
        }

        public void Bankruptcy()
        {
            TempPrizes.RemoveCash();
            TempPrizes.RemovePrizes();
            TotalBankruptcies++;
        }

        public int GetTempCash()
        {
            return TempPrizes.Cash;
        }

        public void DisplayAllInfo(int cursorLeft, int cursorTop)
        {
            Console.CursorLeft = cursorLeft;
            Console.CursorTop = cursorTop;
            Console.WriteLine("Name: {0}", Name);

            Console.CursorLeft = cursorLeft;
            Console.WriteLine("Cash: ${0}", TempPrizes.Cash);

            Console.CursorLeft = cursorLeft;
            Console.WriteLine("Prizes: {0}", TempPrizes.Prizes.Count());
            /*
            if (TempPrizes.Prizes.Count() > 0)
            {
                foreach (string prize in TempPrizes.Prizes)
                {
                    Console.CursorLeft = cursorLeft;
                    Console.WriteLine(prize);
                }
            }
            else
            {
                Console.CursorLeft = cursorLeft;
                Console.WriteLine("None");
            }
            */

            Console.CursorLeft = cursorLeft;
            Console.WriteLine("-------");

            Console.CursorLeft = cursorLeft;
            Console.WriteLine("Total cash: ${0}", TotalCash);

            Console.CursorLeft = cursorLeft;
            Console.WriteLine("Total prizes: {0}", TotalPrizes.Count);
        }

        public void DisplayTotals(int cursorLeft, int cursorTop)
        {
            Console.CursorLeft = cursorLeft;
            Console.CursorTop = cursorTop;
            Console.WriteLine("Name: {0}", Name);

            Console.CursorLeft = cursorLeft;
            Console.WriteLine("-------");

            Console.CursorLeft = cursorLeft;
            Console.WriteLine("Total cash: ${0}", TotalCash);

            Console.CursorLeft = cursorLeft;
            Console.WriteLine("Total prizes: {0}", TotalPrizes.Count);

            if (TotalPrizes.Count() > 0)
            {
                foreach (string prize in TotalPrizes)
                {
                    Console.CursorLeft = cursorLeft;
                    if (prize.Length > 15)
                        Console.WriteLine("{0}...", prize.Substring(0, 15));
                    else
                        Console.WriteLine(prize);
                }
            }
        }
        
        public void CopyTempPrizesToTotalAndDelete()
        {
            TotalCash += TempPrizes.Cash;
            TempPrizes.RemoveCash();

            foreach (string prize in TempPrizes.Prizes)
            {
                TotalPrizes.Add(prize);
            }

            TempPrizes.RemovePrizes();
        }
    }
}
