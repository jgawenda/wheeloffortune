﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WheelOfFortune
{
    class FieldBankrupt : WheelField
    {
        public FieldBankrupt()
        {
            Type = FieldType.Bankrupt;
            Value = -1;
        }

        public override void DisplayToPlayers()
        {
            Console.Write("Bankrut");
        }
    }
}
