﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace WheelOfFortune
{
    class RevealRandomLetters
    {
        private MagicPhrase _magicPhrase;
        private bool _pause = false;
        private bool _stop = false;

        public RevealRandomLetters(MagicPhrase magicPhrase)
        {
            _magicPhrase = magicPhrase;
        }
        
        private void RevealOneRandomLetter(Random random)
        {
            int randomNumber = 0;
            do
            {
                randomNumber = random.Next(0, _magicPhrase.GetPhraseLength());
            } while (!_magicPhrase.RevealTheLetterAt(randomNumber));
        }

        public void AutoRevealLetters()
        {
            Random random = new Random();
            _pause = false;
            
            while (!_magicPhrase.IsPhraseSolved && _magicPhrase.AreThereHiddenChars())
            {
                RevealOneRandomLetter(random);
                Console.Clear();
                Console.WriteLine("\n");
                _magicPhrase.WriteProgress();
                Console.WriteLine("\n\n");

                Thread.Sleep(1500);

                while (_pause)
                {

                }
            }
        }

        public void RequestPause()
        {
            _pause = true;
        }

        public void RequestResume()
        {
            _pause = false;
        }

        public void RequestStop()
        {
            _stop = true;
        }

    }
}
