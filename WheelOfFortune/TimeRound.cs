﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace WheelOfFortune
{
    class TimeRound : Round
    {
        public TimeRound(MagicPhrase magicPhrase, List<PlayerAccount> players)
        {
            _magicPhrase = magicPhrase;
            _playerAccounts = players;
        }

        private void DisplayRules()
        {
            Console.WriteLine("\nIf you want to stop the timer, press:\n\n");
            
            Console.WriteLine("Player 1: A\n");

            Console.WriteLine("Player 2: G\n");

            Console.WriteLine("Player 3: L\n");
        }

        protected override void DisplayRoundName()
        {
            Console.WriteLine("\n\n-----------------------------------------------------------");
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("Time round");
            Console.ResetColor();
            Console.WriteLine("-----------------------------------------------------------");
        }

        public override void Start()
        {
            _magicPhrase.LoadNext();
            Console.Clear();

            DisplayRoundName();
            DisplayRules();

            Console.ReadLine();

            Console.Clear();

            Console.WriteLine("\n");

            _magicPhrase.WriteProgress();

            Console.WriteLine("\n\n\nPress enter to begin");
            Console.ReadLine();

            Console.Clear();

            TimeRoundProceed timeRoundProceed = new TimeRoundProceed(_magicPhrase, _playerAccounts, 1000, "Zestaw gier planszowych");
            Thread timeRoundProceedThread = new Thread(new ThreadStart(timeRoundProceed.Begin));
            timeRoundProceed.AddThread(timeRoundProceedThread);
            timeRoundProceedThread.Start();

            timeRoundProceedThread.Join();

            Console.Clear();
            DisplayPlayersTotals();
            Console.ReadLine();
        }
    }
}
