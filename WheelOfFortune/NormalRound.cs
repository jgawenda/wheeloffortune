﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WheelOfFortune
{
    class NormalRound : Round
    {
        private Wheel _wheel;

        //int representing list index of player who should start round
        private int _whoShouldStartRound;

        private void NextPlayerToStart()
        {
            _whoShouldStartRound++;

            if (_whoShouldStartRound >= _playerAccounts.Count)
                _whoShouldStartRound = 0;
        }

        public NormalRound(MagicPhrase magicPhrase, Wheel wheel, List<PlayerAccount> playerAccounts)
        {
            _magicPhrase = magicPhrase;
            _wheel = wheel;
            _playerAccounts = playerAccounts;
            _whoShouldStartRound = 0;
        }

        private bool PlayerTurn(PlayerAccount player)
        {
            bool operationAllowed = false;
            bool userGetNextTurn = false;

            do
            {
                Console.Clear();
                DisplayPlayersInfo();

                Console.WriteLine("\n\n---------------------------------------------------------------------");
                Console.BackgroundColor = player.Color;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.WriteLine("Now it's {0}'s turn", player.Name);
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.BackgroundColor = ConsoleColor.Black;
                Console.WriteLine("---------------------------------------------------------------------");

                Console.WriteLine("\nPhrase:\n");
                _magicPhrase.WriteProgress();

                Console.WriteLine("\n\n\n----\nWhat is your choice?:");

                if (!_magicPhrase.AreThereUnrevealedConsonants())
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.WriteLine("1. Spin the wheel");
                Console.ForegroundColor = ConsoleColor.Gray;

                Console.WriteLine("2. Solve the puzzle");

                if (!_magicPhrase.AreThereUnrevealedVowels() || player.TempPrizes.Cash < 200)
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.WriteLine("3. Buy a vowel");
                Console.ForegroundColor = ConsoleColor.Gray;

                int userChoice = PlayerActions.ChoseAction();

                switch (userChoice)
                {
                    case 1:
                        if (!_magicPhrase.AreThereUnrevealedConsonants())
                        {
                            operationAllowed = false;
                            Console.WriteLine("Operation not allowed!");
                            Console.ReadLine();
                        }
                        else
                        {
                            operationAllowed = true;
                            //Console.WriteLine("Press enter to spin the wheel");
                            //Console.ReadLine();
                            userGetNextTurn = PlayerActions.SpinWheel(_wheel, _magicPhrase, player);
                        }
                        break;
                    case 2:
                        operationAllowed = true;
                        Console.WriteLine("\n[Solving puzzle]");
                        Console.Write("Enter the correct phrase: ");

                        if (_magicPhrase.SolveThePhrase(Console.ReadLine()))
                        {
                            userGetNextTurn = false;
                            player.CopyTempPrizesToTotalAndDelete();
                        }
                        else
                            Console.WriteLine("Wrong phrase!");

                        break;
                    case 3:
                        if (!_magicPhrase.AreThereHiddenChars() || player.TempPrizes.Cash < 200)
                        {
                            operationAllowed = false;
                            Console.WriteLine("Operation not allowed!");
                            Console.ReadLine();
                        }
                        else
                        {
                            operationAllowed = true;
                            userGetNextTurn = PlayerActions.BuyVowel(_magicPhrase, player);
                        }
                        break;
                    default:
                        break;
                }
            } while (!operationAllowed);

            Console.WriteLine("\n\n");
            _magicPhrase.WriteProgress();
            Console.WriteLine("\n");
            Console.ReadLine();

            return userGetNextTurn;
        }

        protected override void DisplayRoundName()
        {
            Console.WriteLine("\n\n-----------------------------------------------------------");
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("Normal round");
            Console.ResetColor();
            Console.WriteLine("-----------------------------------------------------------");
        }

        public override void Start()
        {
            _magicPhrase.LoadNext();

            Console.Clear();
            DisplayRoundName();
            //DisplayRoundNumber(i + 1);
            Console.ReadLine();

            int playerCounter = _whoShouldStartRound;

            do
            {

                if (playerCounter >= _playerAccounts.Count)
                    playerCounter = 0;

                Console.Clear();
                Console.WriteLine("\n\n-----------------------------------------------------------");
                Console.BackgroundColor = _playerAccounts.ElementAt(playerCounter).Color;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.WriteLine("Now it's {0}'s turn", _playerAccounts.ElementAt(playerCounter).Name);
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.BackgroundColor = ConsoleColor.Black;
                Console.WriteLine("-----------------------------------------------------------");
                Console.ReadLine();

                //repeat one player turn
                while (PlayerTurn(_playerAccounts.ElementAt(playerCounter)))
                {
                    /*
                    if (_magicPhrase.IsGameOverByNoHiddenCharsLeft())
                        break;
                        */
                }

                playerCounter++;

            } while (!_magicPhrase.IsPhraseSolved);

            RemoveAllTempPrizes();

            _wheel.ResetTheFields();

            NextPlayerToStart();

            DisplayPlayersTotals();

            Console.ReadLine();
        }

        public void Start(int howManyRounds)
        {
            for (int i = 0; i < howManyRounds; i++)
            {
                Start();
            }
        }

    }
}
