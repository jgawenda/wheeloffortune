﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Timers;

namespace WheelOfFortune
{
    class FinalSolvePhrase
    {
        private Thread _thread;
        private MagicPhrase _magicPhrase;
        private bool _playerHasTimeToSolve;

        public FinalSolvePhrase(MagicPhrase magicPhrase)
        {
            _magicPhrase = magicPhrase;
            _playerHasTimeToSolve = true;
        }

        public void AddThread(Thread thread)
        {
            _thread = thread;
        }

        public void UserSolveThePuzzle()
        {

            System.Timers.Timer timer = new System.Timers.Timer(10000);
            timer.Elapsed += TimerHandler;

            timer.Start();
            
            string userGuess = "";

            while (_playerHasTimeToSolve)
            {
                Console.Write("Solve the phrase: ");
                userGuess = Console.ReadLine();

                if (_magicPhrase.SolveThePhrase(userGuess))
                {
                    timer.Stop();
                    break;
                }
            }
        }

        private void TimerHandler(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("Time's UP.");
            _playerHasTimeToSolve = false;
            System.Timers.Timer timer = (System.Timers.Timer)sender;
            timer.Stop();

            _thread.Abort();
        }
    }
}
