﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WheelOfFortune
{
    abstract class WheelField
    {
        public enum FieldType
        {
            Cash,
            CashOrHidden,
            Prize,
            Stop,
            Bankrupt
        }

        public FieldType Type { get; protected set; }

        public int Value { get; protected set; }

        public abstract void DisplayToPlayers();
    }
}
