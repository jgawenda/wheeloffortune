﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace WheelOfFortune
{
    class RandomRound : Round
    {
        private Wheel _wheel;
        private int _valueToWinHere;
        private bool _userHasTimeToSolve;

        public RandomRound(List<PlayerAccount> playerAccounts, Wheel wheel, MagicPhrase magicPhrase)
        {
            _playerAccounts = playerAccounts;
            _wheel = wheel;
            _magicPhrase = magicPhrase;
            _userHasTimeToSolve = true;
        }

        protected override void DisplayRoundName()
        {
            Console.WriteLine("\n\n-----------------------------------------------------------");
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("Random round");
            Console.ResetColor();
            Console.WriteLine("-----------------------------------------------------------");
        }

        private void DrawRandomCash()
        {
            WheelField field;

            do
            {
                field = _wheel.SpinTheWheel();
            } while (field.Type != WheelField.FieldType.Cash);

            _valueToWinHere = field.Value;
            Console.WriteLine("Our multiplier is ${0}", _valueToWinHere);
        }

        private void PlayerGuessThePhrase(PlayerAccount player, Timer timer)
        {
            timer.Interval = 15000;
            timer.Start();
            Console.WriteLine("You have 15 seconds to solve the phrase");
            Console.Write("Solve phrase: ");

            string userGuess = Console.ReadLine();

            if (_userHasTimeToSolve && _magicPhrase.SolveThePhrase(userGuess))
            {
                timer.Stop();
                Console.WriteLine("CONGRATULATIONS!");
                player.CopyTempPrizesToTotalAndDelete();
            }
            else
                timer.Stop();

        }

        private void PlayerTurn(PlayerAccount player, Timer timer)
        {
            Console.Clear();
            _userHasTimeToSolve = true;

            Console.WriteLine("\n\n---------------------------------------------");
            Console.BackgroundColor = player.Color;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("Now is {0}'s turn", player.Name);
            Console.ResetColor();
            Console.WriteLine("---------------------------------------------\n\n");

            _magicPhrase.WriteProgress();
            Console.WriteLine("\n\n");

            if (_magicPhrase.AreThereHiddenChars())
            {
                char userChoice = PlayerActions.GetCharFromUser();
                int howManyHits = _magicPhrase.GuessTheConsonant(userChoice);

                bool didPlayerShotVowel = false;

                if (howManyHits == 0)
                {
                    didPlayerShotVowel = true;
                    howManyHits = _magicPhrase.GuessTheVowel(userChoice);
                }

                if (howManyHits > 0)
                {
                    Console.Clear();

                    if (!didPlayerShotVowel)
                    {
                        Console.WriteLine("\nLetter {0} is {1} time(s). {2} x {1} = ${3}", Char.ToUpper(userChoice), howManyHits, _valueToWinHere, _valueToWinHere * howManyHits);
                        player.AddCashTemporarily(_valueToWinHere * howManyHits);
                    }
                    else
                    {
                        Console.WriteLine("\nLetter {0} is {1} time(s).", Char.ToUpper(userChoice), howManyHits);
                        Console.WriteLine("But it is vowel, so you get no cash for this.");
                    }

                    Console.ReadLine();

                    Console.WriteLine("\n");
                    _magicPhrase.WriteProgress();
                    Console.WriteLine("\n\n\n");

                    PlayerGuessThePhrase(player, timer);

                }

            }
            else
            {
                PlayerGuessThePhrase(player, timer);
            }
            
        }

        private void ProceedWithRandomRound()
        {
            Timer timer = new Timer();
            timer.Elapsed += Timer_Elapsed;

            int playerTurnCounter = 0;

            do
            {
                if (playerTurnCounter >= _playerAccounts.Count)
                    playerTurnCounter = 0;

                PlayerTurn(_playerAccounts.ElementAt(playerTurnCounter), timer);

                playerTurnCounter++;

            } while (!_magicPhrase.IsPhraseSolved);

            RemoveAllTempPrizes();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _userHasTimeToSolve = false;

            Timer timer = (Timer)sender;
            timer.Stop();

            Console.WriteLine("\n\nTime's UP.");
        }

        public override void Start()
        {
            _magicPhrase.LoadNext();
            Console.Clear();
            DisplayRoundName();

            Console.ReadLine();

            DrawRandomCash();
            Console.ReadLine();

            Console.Clear();
            ProceedWithRandomRound();

            Console.ReadLine();
            Console.Clear();

            DisplayPlayersTotals();
            Console.ReadLine();
        }
    }
}
