﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WheelOfFortune
{
    class FieldPrize : WheelField
    {
        public string Text { get; }

        public FieldPrize(string text)
        {
            Type = FieldType.Prize;
            Value = 0;
            Text = text;
        }

        public override void DisplayToPlayers()
        {
            Console.Write("Nagroda");
        }
    }
}
