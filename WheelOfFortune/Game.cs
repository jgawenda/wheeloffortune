﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpingMethods;

namespace WheelOfFortune
{
    public class Game
    {
        private int _howManyPlayers = 3;

        private MagicPhrase _magicPhrase = new MagicPhrase();
        private Wheel _wheel = new Wheel();

        private List<PlayerAccount> _playerAccounts = new List<PlayerAccount>();
        
        private void CreatePlayers()
        {
            string tempName = "";

            List<ConsoleColor> colors = new List<ConsoleColor> { ConsoleColor.Magenta, ConsoleColor.Cyan, ConsoleColor.Yellow };

            for (int i=0; i<_howManyPlayers; i++)
            {
                do
                {
                    Console.Write("Enter name for player {0}: ", i + 1);
                    tempName = Console.ReadLine();
                } while (tempName == "");
                
                _playerAccounts.Add(new PlayerAccount(tempName, colors.ElementAt(i)));
            }
        }
        
        private void DisplayRoundNumber(int number)
        {
            Console.WriteLine("\n\n-----------------------------------------------------------");
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("Round number: {0}", number);
            Console.ResetColor();
            Console.WriteLine("-----------------------------------------------------------");
        }
        
        private void DisplayWelcome()
        {
            Console.WriteLine("--------------------------------------------------------------");
            Console.CursorLeft = 15;
            Console.WriteLine("Welcome to Wheel Of Fortune!");
            Console.WriteLine("--------------------------------------------------------------");
        }

        public Game()
        {
            Console.WindowWidth = 120;
            Console.WindowHeight = 50;
            
            DisplayWelcome();
            Console.ReadLine();

            CreatePlayers();

            _magicPhrase.Prepare();
        }

        public void Start()
        {
            //int roundNumber = 1;
            //1st: TimeRound
            TimeRound timeRound = new TimeRound(_magicPhrase, _playerAccounts);
            timeRound.Start();

            //2nd and 3rd round: NormalRound
            NormalRound normalRound = new NormalRound(_magicPhrase, _wheel, _playerAccounts);
            normalRound.Start(2);

            //4th round: TimeRound
            timeRound.Start();

            //5th round: RandomRound
            RandomRound randomRound = new RandomRound(_playerAccounts, _wheel, _magicPhrase);
            randomRound.Start();

            //6th round: NormalRound
            normalRound.Start();

            //7th round: FinalRound
            FinalRound finalRound = new FinalRound(_playerAccounts, _magicPhrase);
            finalRound.Start();
            
        }
    }
}
