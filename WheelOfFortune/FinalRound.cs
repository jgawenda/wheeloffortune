﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
//using System.Threading;

namespace WheelOfFortune
{
    class FinalRound : Round
    {
        private PlayerAccount _winner;
        private FinalWheel _finalWheel;
        private FinalField _prize;
        private bool _playerHasTimeToSolve;

        public FinalRound(List<PlayerAccount> playerAccounts, MagicPhrase magicPhrase)
        {
            _playerAccounts = playerAccounts;
            _magicPhrase = magicPhrase;
            _finalWheel = new FinalWheel();
            _playerHasTimeToSolve = true;
            
            DrawWinner();
        }

        protected override void DisplayRoundName()
        {
            Console.WriteLine("\n\n-----------------------------------------------------------");
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("GRAND FINAL!");
            Console.ResetColor();
            Console.WriteLine("-----------------------------------------------------------");
        }

        private void DisplayWelcome()
        {
            Console.WriteLine("\n\nWelcome to GRAND FINAL!");
            Console.BackgroundColor = _winner.Color;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("\nNow {0} will spin the final wheel", _winner.Name);
            Console.ResetColor();
            Console.WriteLine("-------------------");
            Console.Write("\n\n\nPress enter to spin the wheel");
            Console.ReadLine();
        }

        private void DrawPrize()
        {
            _prize = _finalWheel.SpinTheWheel();
            
            Console.WriteLine("\n\nYour prize is saved.");

            Console.ReadLine();
        }

        private void DrawWinner()
        {
            //see which player has the most money
            _winner = _playerAccounts.ElementAt(0);

            for (int i = 1; i < _playerAccounts.Count(); i++)
            {
                if (_playerAccounts.ElementAt(i).TotalCash > _winner.TotalCash)
                    _winner = _playerAccounts.ElementAt(i);
            }

            Console.Clear();

            Console.BackgroundColor = _winner.Color;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("The winner is: {0}", _winner.Name);
            Console.ResetColor();
            Console.ReadLine();

            Console.Clear();
        }

        private void RevealStandardLetters()
        {
            Console.WriteLine("We give you standard set of letters: [R] [S] [T] [L] [N] [E]");
            Console.ReadLine();
            
            char[] standardLetters = new char[] { 'r', 's', 't', 'l', 'n' };

            foreach (char character in standardLetters)
            {
                _magicPhrase.GuessTheConsonant(character);
            }

            _magicPhrase.GuessTheVowel('e');
        }

        private void UserGuessTheLetters()
        {
            Console.WriteLine("Now enter your letters\n");
            
            for (int i=0; i<3; i++)
            {
                Console.WriteLine("Consonant {0}/3:", i + 1);
                PlayerActions.GuessTheConsonant(_magicPhrase, false);
                Console.WriteLine("\n---------");
            }

            Console.WriteLine("[Guessing Vowel]\n");
            _magicPhrase.GuessTheVowel(PlayerActions.GetCharFromUser());
        }
        
        private void UserSolveThePuzzle()
        {
            
            Timer timer = new Timer(20000);
            timer.Elapsed += TimerHandler;
            
            timer.Start();
            
            string userGuess = "";

            while (_playerHasTimeToSolve)
            {
                Console.Write("Solve the phrase: ");
                userGuess = Console.ReadLine();

                if (_playerHasTimeToSolve && _magicPhrase.SolveThePhrase(userGuess))
                {
                    timer.Stop();
                    break;
                }
                
            }
            
        }
        
        private void TimerHandler(object sender, ElapsedEventArgs e)
        {
            Console.Clear();
            Console.Beep();
            Console.WriteLine("\n\nTime's UP.");
            _playerHasTimeToSolve = false;
            Timer timer = (Timer)sender;
            timer.Stop();
        }
        
        private void ShowTheFinalPrize(bool didUserWin)
        {
            if (didUserWin)
                Console.Write("You've won... ");
            else
                Console.Write("The prize was... ");

            Console.ReadLine();
            _prize.Reveal();
        }

        private void AddFinalPrizeToPlayer()
        {
            if (_prize.Value > 0)
                _winner.TempPrizes.AddCash(_prize.Value);
            else
                _winner.TempPrizes.AddPrize(_prize.Prize);

            _winner.CopyTempPrizesToTotalAndDelete();
        }
        
        public override void Start()
        {
            _magicPhrase.LoadNext();

            Console.Clear();
            DisplayWelcome();
            
            DrawPrize();

            Console.Clear();
            Console.WriteLine("Now let's discover your Phrase:\n");
            _magicPhrase.WriteProgress();
            Console.WriteLine("\n\n");
            Console.ReadLine();
            
            RevealStandardLetters();
            Console.Clear();
            Console.WriteLine("\n");
            _magicPhrase.WriteProgress();
            Console.WriteLine("\n\n");
            Console.ReadLine();

            UserGuessTheLetters();

            Console.Clear();

            Console.WriteLine("\n");
            _magicPhrase.WriteProgress();

            Console.WriteLine("\n\n");
            //Console.ReadLine();

            /* Thread approach
            FinalSolvePhrase solvePhrase = new FinalSolvePhrase(_magicPhrase);
            Thread newThread = new Thread(new ThreadStart(solvePhrase.UserSolveThePuzzle));
            solvePhrase.AddThread(newThread);
            newThread.Start();

            newThread.Join();
            */

            UserSolveThePuzzle();
            
            if (_magicPhrase.IsPhraseSolved)
            {
                AddFinalPrizeToPlayer();
                Console.WriteLine("CONGRATULATIONS!");
            }
            else
            {
                _magicPhrase.SolveThePhraseAutomatically();
                Console.WriteLine("Sorry you didn't win :(");
            }

            Console.WriteLine("\n\n");
            _magicPhrase.WriteProgress();
            Console.WriteLine("\n\n");

            Console.ReadLine();
            ShowTheFinalPrize(_magicPhrase.IsPhraseSolved);

            Console.ReadLine();

            Console.Clear();

            _winner.DisplayTotals(20, 10);

            Console.ReadLine();

        }

    }
}
