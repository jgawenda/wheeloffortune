﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WheelOfFortune
{
    class Wheel
    {
        private List<WheelField> _wheelFields = new List<WheelField>();

        public Wheel()
        {
            PrepareWheelFields();
        }
        
        private void PrepareWheelFields()
        {
            /*
             * Bankrupt
             * 2500
             * 250
             * Prize
             * 350
             * 200
             * 300
             * Prize
             * Hidden with Bankrupt
             * 200
             * 1000
             * Bankrupt
             * 2000
             * 200
             * 350
             * 550
             * 200
             * Hidden with 3000
             * Stop
             * 300
             * 250
             * 500
             * 150
             * Prize
            */

            int[] fields = new int[] { -1, 2500, 250, 9999, 350, 200, 300, 9999, 66, 200, 1000, -1, 2000, 200, 350, 550, 200, 77, 0, 300, 250, 500, 150, 9999 };
            string[] prizes = new string[] { "Szwajcarski zegarek", "Tydzień w SPA", "Chłodziarko-zamrażarka" };

            int countPrizes = 0;
            foreach (int value in fields)
            {
                switch (value)
                {
                    case -1:
                        _wheelFields.Add(new FieldBankrupt());
                        break;
                    case 9999:
                        _wheelFields.Add(new FieldPrize(prizes[countPrizes]));
                        countPrizes++;
                        break;
                    case 66:
                        _wheelFields.Add(new FieldCashOrHidden(false));
                        break;
                    case 77:
                        _wheelFields.Add(new FieldCashOrHidden(true));
                        break;
                    case 0:
                        _wheelFields.Add(new FieldStop());
                        break;
                    default:
                        _wheelFields.Add(new FieldCash(value));
                        break;
                }
            }

        }

        public void ResetTheFields()
        {
            _wheelFields.Clear();

            PrepareWheelFields();
        }
        
        public WheelField SpinTheWheel()
        {

            DateTime end = DateTime.Now.AddSeconds(5);

            int counter = 0;

            while (DateTime.Now < end)
            {
                Console.Clear();

                if (counter == _wheelFields.Count)
                    counter = 0;

                _wheelFields.ElementAt(counter).DisplayToPlayers();
                Console.WriteLine("");
                //Console.WriteLine("Press Enter to stop the wheel or wait few seconds");

                if (Console.KeyAvailable)
                {
                    var key = Console.ReadKey();
                    if (key.Key == ConsoleKey.Enter)
                    {
                        Console.Clear();
                        //_wheelFields.ElementAt(counter).DisplayToPlayers();
                        return _wheelFields.ElementAt(counter);
                    }
                }

                counter++;
            }

            return _wheelFields.ElementAt(counter);
        }

        public void DisplayAllFields()
        {
            foreach (WheelField field in _wheelFields)
            {
                field.DisplayToPlayers();
                Console.Write(" | ");
            }
        }

        public void ChangeFieldToCash(WheelField field, int newValue)
        {
            int elementID = _wheelFields.IndexOf(field);
            
            if (elementID != -1)
                _wheelFields[elementID] = new FieldCash(newValue);

        }

    }
}
