﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WheelOfFortune
{
    class FieldCash : WheelField
    {
        public FieldCash(int value)
        {
            Type = FieldType.Cash;
            Value = value;
        }

        public override void DisplayToPlayers()
        {
            Console.Write(Value);
        }
    }
}
