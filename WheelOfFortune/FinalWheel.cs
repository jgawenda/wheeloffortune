﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WheelOfFortune
{
    class FinalWheel
    {
        private List<FinalField> _fields = new List<FinalField>();

        private void PrepareFields()
        {
            /*
             * 24 fields:
             * 
             * car x 2
             * 50000 x 2 ok
             * 25000 x 2 ok
             * 20000 x 3 ok
             * 15000 x 4 ok
             * 10000 x 5 ok
             * 5000 x 6 ok
            */

            int[] fields = new int[] { 15000, 5000, 20000, 50000, 10000, 10000, 0, 15000, 20000, 5000, 25000, 5000, 10000, 10000, 20000, 15000, 5000, 0, 50000, 15000, 5000, 5000, 25000, 10000 };
            
            foreach (int value in fields)
            {
                switch (value)
                {
                    case 0:
                        _fields.Add(new FinalField("CAR: TOYOTA YARIS 2017"));
                        break;
                    default:
                        _fields.Add(new FinalField(value));
                        break;
                }
            }

        }
        
        public FinalWheel()
        {
            PrepareFields();
        }

        public FinalField SpinTheWheel()
        {

            DateTime end = DateTime.Now.AddSeconds(5);

            int counter = 0;

            while (DateTime.Now < end)
            {
                Console.Clear();

                if (counter == _fields.Count)
                    counter = 0;

                int modulo = counter % 2;

                switch (modulo)
                {
                    case 0:
                        //Console.BackgroundColor = ConsoleColor.White;
                        //Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine("[ ]");
                        break;
                    default:
                        //Console.BackgroundColor = ConsoleColor.Black;
                        //Console.ForegroundColor = ConsoleColor.Gray;
                        Console.WriteLine("[  ]");
                        break;
                }
                
                if (Console.KeyAvailable)
                {
                    var key = Console.ReadKey();
                    if (key.Key == ConsoleKey.Enter)
                    {
                        Console.Clear();
                        //_wheelFields.ElementAt(counter).DisplayToPlayers();
                        return _fields.ElementAt(counter);
                    }
                }

                counter++;
            }

            return _fields.ElementAt(counter);
        }
        
    }
}
