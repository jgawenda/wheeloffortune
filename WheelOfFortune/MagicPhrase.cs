﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WheelOfFortune
{
    class MagicPhrase
    {
        private string _phrase = "";
        public string Category { get; private set; } = "";
        private char[] _phraseProgress;
        private char[] _consonants = new char[] { 'b', 'c', 'ć', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'ł', 'm', 'n', 'ń', 'p', 'q', 'r', 's', 'ś', 't', 'v', 'w', 'x', 'z', 'ż', 'ź' };
        private char[] _vowels = new char[] { 'a', 'e', 'y', 'i', 'o', 'ą', 'ę', 'u', 'ó' };
        private PhrasesReader _phrasesReader;

        public bool IsPhraseSolved { get; private set; } = false;

        private int RevealConsonant(char character)
        {
            int counter = 0;

            for (int i=0; i<_phrase.Length; i++)
            {
                if (_phrase[i] == character)
                {
                    _phraseProgress[i] = character;
                    counter++;
                }
            }

            return counter;
        }
        
        public void Prepare()
        {
            Console.Clear();
            Console.WriteLine("Enter path to file with phrases");
            Console.Write(@"(or leave empty to use default D:\phrases\phrases.txt): ");

            string filePath = Console.ReadLine();

            if (filePath == "")
                filePath = @"D:\phrases\phrases.txt";

            _phrasesReader = new PhrasesReader(filePath);

            Console.ReadLine();

        }

        private void PrepareProgress()
        {
            IsPhraseSolved = false;
            _phraseProgress = new char[_phrase.Length];

            for (int i = 0; i < _phraseProgress.Count(); i++)
            {

                if (IsSpecialChar(_phrase[i]))
                    _phraseProgress[i] = _phrase[i];
                else
                    _phraseProgress[i] = '_';

            }
        }

        private bool IsSpecialChar(char character)
        {
            if (_consonants.Contains(character) || _vowels.Contains(character))
                return false;
            else
                return true;
        }

        public void LoadNext()
        {
            string[] tempArray;

            do
            {
                tempArray = _phrasesReader.GetNextRandomPhrase();

                if (tempArray[0] != "" && tempArray[1] != "")
                {
                    _phrase = tempArray[0];
                    Category = tempArray[1];

                    PrepareProgress();

                    //Console.WriteLine("New phrase loaded!");
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("We ran out of phrases to solve. Please provide another file with phrases.");
                    Console.WriteLine("Press enter to continue...");
                    Console.ReadLine();
                    Prepare();
                }

            } while (tempArray[0] == "" || tempArray[1] == "");
            
        }

        public void WriteProgress()
        {
            Console.Write(" ");

            int counter = 0;
            foreach (char character in _phraseProgress)
            {
                if (character != ' ')
                    Console.Write("{0} ", Char.ToUpper(character));
                else
                {
                    if (counter > 46)
                    {
                        Console.WriteLine("\n");
                        counter = 0;
                    }

                    Console.Write(" {0}", character);
                }

                counter++;
            }

            Console.Write("\n\ncategory: [ {0} ]", Category);
        }

        public int GuessTheConsonant(char consonant)
        {
            if (!_consonants.Contains(consonant))
                return 0;

            if (_phraseProgress.Contains(consonant))
                return 0;
            else
            {
                return RevealConsonant(consonant);
            }
        }

        public int GuessTheVowel(char vowel)
        {
            if (!_vowels.Contains(vowel))
                return 0;

            if (_phraseProgress.Contains(vowel))
                return 0;
            else
            {
                return RevealVowel(vowel);
            }

        }

        private int RevealVowel(char vowel)
        {
            int counter = 0;

            for (int i=0; i<_phrase.Length; i++)
            {
                if (_phrase[i] == vowel)
                {
                    counter++;
                    _phraseProgress[i] = vowel;
                }
            }

            return counter;
        }

        public bool AreThereHiddenChars()
        {
            foreach (char character in _phraseProgress)
            {
                if (character == '_')
                    return true;
            }

            return false;
        }

        public bool AreThereUnrevealedConsonants()
        {
            foreach (char character in _consonants)
            {
                if (!_phraseProgress.Contains(character))
                {
                    if (_phrase.Contains(character))
                        return true;
                }
            }

            return false;
        }

        public bool AreThereUnrevealedVowels()
        {
            foreach (char character in _vowels)
            {
                if (!_phraseProgress.Contains(character))
                {
                    if (_phrase.Contains(character))
                        return true;
                }
            }

            return false;
        }

        public bool SolveThePhrase(string guessedPhrase)
        {
            if (_phrase == guessedPhrase)
            {
                IsPhraseSolved = true;

                Console.Clear();
                Console.WriteLine("Phrase solved!");

                for (int i = 0; i < _phraseProgress.Count(); i++)
                {
                    _phraseProgress[i] = _phrase[i];
                }

                //Console.WriteLine(_phrase);

                return true;
            }
            else
                return false;
        }
        
        public void SolveThePhraseAutomatically()
        {
            for (int i = 0; i < _phraseProgress.Count(); i++)
            {
                _phraseProgress[i] = _phrase[i];
            }
            
        }

        //return true if revealing was succesful, otherwise (if it was already revealed) return false
        public bool RevealTheLetterAt(int number)
        {
            if (_phraseProgress[number] == '_')
            {
                _phraseProgress[number] = _phrase[number];
                return true;
            }
            else
                return false;

        }

        public int GetPhraseLength()
        {
            return _phraseProgress.Count();
        }
    }
}
