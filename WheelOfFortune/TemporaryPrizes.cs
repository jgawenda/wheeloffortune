﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WheelOfFortune
{
    class TemporaryPrizes
    {
        public int Cash { get; private set; }
        public List<string> Prizes { get; private set; }

        public TemporaryPrizes()
        {
            Cash = 0;
            Prizes = new List<string>();
        }

        public void AddCash(int amount)
        {
            Cash += amount;
        }

        public void RemoveCash()
        {
            Cash = 0;
        }

        public void AddPrize(string name)
        {
            Prizes.Add(name);
        }

        public void RemovePrizes()
        {
            Prizes.Clear();
        }

        public void SubtractCash(int amount)
        {
            if (amount > Cash)
                RemoveCash();
            else
            {
                Cash -= amount;
            }
        }
    }
}
