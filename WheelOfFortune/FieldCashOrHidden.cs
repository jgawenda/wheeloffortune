﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WheelOfFortune
{
    class FieldCashOrHidden : WheelField
    {
        private int _cashHiddenUnder;

        public FieldCashOrHidden(bool isThereMoreCashUnder)
        {
            Type = FieldType.CashOrHidden;
            Value = 500;

            if (isThereMoreCashUnder)
                _cashHiddenUnder = 3000;
            else
                _cashHiddenUnder = -1;

        }

        public int GetHiddenCash()
        {
            return _cashHiddenUnder;
        }

        public override void DisplayToPlayers()
        {
            Console.Write("500?");
        }
    }
}
