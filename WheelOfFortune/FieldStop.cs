﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WheelOfFortune
{
    class FieldStop : WheelField
    {
        public FieldStop()
        {
            Value = 0;
            Type = FieldType.Stop;
        }

        public override void DisplayToPlayers()
        {
            Console.Write("STOP");
        }
    }
}
