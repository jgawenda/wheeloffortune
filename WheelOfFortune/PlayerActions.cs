﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpingMethods;

namespace WheelOfFortune
{
    class PlayerActions
    {
        public static int ChoseAction()
        {
            int output = 0;
            string userInput = "";

            do
            {
                Console.Write("Your choice: ");
                userInput = Console.ReadLine();
            } while (!Int32.TryParse(userInput, out output));

            return output;
        }

        public static char GetCharFromUser()
        {
            char userPickChar = ' ';
            string userPick = "";

            do
            {
                Console.Write("Your choice: ");
                userPick = Console.ReadLine();
            } while (userPick.Length != 1 || !Char.TryParse(userPick, out userPickChar));

            return userPickChar;
        }
        
        public static int GuessTheConsonant(MagicPhrase magicPhrase, bool displayProgress)
        {
            Console.WriteLine("[Guess the consonant]\n");

            if (displayProgress)
            {
                magicPhrase.WriteProgress();
                Console.WriteLine("\n\n----------------------");
            }

            char userPick = GetCharFromUser();
            int howManyHits = magicPhrase.GuessTheConsonant(userPick);

            //Console.Clear();

            /*
            if (howManyHits > 0)
                Console.WriteLine("There is {0} time(s) letter \"{1}\"", howManyHits, userPick);
            else
                Console.WriteLine("There is no letter {0} in phrase or it was already revealed", userPick);
            */

            return howManyHits;
        }

        //return true if there is some cash under
        private static bool RevealTheFieldAndProceed(Wheel wheel, FieldCashOrHidden field, PlayerAccount playerAccount)
        {
            if (field.GetHiddenCash() > 0)
            {
                playerAccount.TempPrizes.AddCash(field.GetHiddenCash());
                //change the field from FieldCashOrHidden to FieldCash
                wheel.ChangeFieldToCash(field, 600);
                Console.Write("${0}!", field.GetHiddenCash());
                return true;
            }
            else
            {
                playerAccount.Bankruptcy();
                wheel.ChangeFieldToCash(field, 600);
                Console.Write("BANKRUPT!");
                return false;
            }
                
        }

        //return true if player deserve another turn
        public static bool SpinWheel(Wheel wheel, MagicPhrase magicPhrase, PlayerAccount playerAccount)
        {
            var drawnField = wheel.SpinTheWheel();
            drawnField.DisplayToPlayers();
            Console.ReadLine();

            if (drawnField is FieldCash)
            {
                FieldCash field = (FieldCash)drawnField;
                int howManyConsonantsHit = GuessTheConsonant(magicPhrase, true);
                Console.Clear();

                if (howManyConsonantsHit > 0)
                {
                    playerAccount.TempPrizes.AddCash(field.Value * howManyConsonantsHit);

                    return true;
                }
                else
                    return false;
            }
            else if (drawnField is FieldCashOrHidden)
            {
                //if player draw 500?
                FieldCashOrHidden field = (FieldCashOrHidden)drawnField;
                Console.WriteLine("Do you want to keep the {0} or reveal the under value?", field.Value);
                Console.WriteLine("1. Keep the {0}", field.Value);
                Console.WriteLine("2. Reveal value under");

                int userChoice = 0;
                do
                {
                    Console.Write("\n----\nYour choice: ");
                    userChoice = Helper.GetIntFromUser();
                } while (userChoice < 0 || userChoice > 2);

                int howManyConsonantsHit = GuessTheConsonant(magicPhrase, true);
                Console.Clear();

                if (howManyConsonantsHit > 0)
                {
                    switch (userChoice)
                    {
                        case 1:
                            playerAccount.TempPrizes.AddCash(field.Value);
                            return true;
                        case 2:
                            Console.Write("And the hidden value is... ");
                            Console.ReadLine();
                            bool doIHaveAnotherTurn = RevealTheFieldAndProceed(wheel, field, playerAccount);
                            Console.ReadLine();
                            return doIHaveAnotherTurn;
                    }
                }
                else
                    return false;
                
            }
            else if (drawnField is FieldBankrupt)
            {
                playerAccount.Bankruptcy();
                return false;
            }
            else if (drawnField is FieldPrize)
            {
                FieldPrize field = (FieldPrize)drawnField;
                Console.WriteLine("The prize is: {0}", field.Text);
                Console.ReadLine();

                int howManyConsonantHit = GuessTheConsonant(magicPhrase, true);
                Console.Clear();

                if (howManyConsonantHit > 0)
                {
                    playerAccount.TempPrizes.AddPrize(field.Text);
                    wheel.ChangeFieldToCash(field, 600);
                    return true;
                }
                else
                    return false;
            }
            else if (drawnField is FieldStop)
            {
                return false;
            }

            return false;
        }
        
        //return true if player guessed right vowel
        public static bool BuyVowel(MagicPhrase magicPhrase, PlayerAccount playerAccount)
        {
            Console.WriteLine("\n\n----------------------");

            Console.WriteLine("Which vowel would you like to buy?");
            char userPick = GetCharFromUser();
            
            playerAccount.TempPrizes.SubtractCash(200);

            int howManyHits = magicPhrase.GuessTheVowel(userPick);

            Console.Clear();

            if (howManyHits > 0)
            {
                //Console.WriteLine("There is {0} time(s) letter \"{1}\"", howManyHits, userPick);
                return true;
            }
            else
            {
                //Console.WriteLine("There is no letter {0} in phrase or it was already revealed", userPick);
                return false;
            }
        }
    }
}
