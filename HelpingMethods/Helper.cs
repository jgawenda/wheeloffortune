﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpingMethods
{
    public class Helper
    {
        public static int GetIntFromUser()
        {
            int output = 0;
            string userInput = "";

            do
            {
                userInput = Console.ReadLine();
            } while (!Int32.TryParse(userInput, out output));

            return output;
        }
    }
}
